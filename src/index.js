"use strict";
// Développe une application en ligne de commande en TypeScript qui permet à l'utilisateur de convertir des montants entre l'euro (EUR), le dollar canadien (CAD) et le yen japonais (JPY), ainsi que de calculer les frais de livraison et les frais de douane des colis.
// 1 EUR = 1.5 CAD = 130 JPY
// 1 CAD = 0.67 EUR = 87 JPY
// 1 JPY = 0.0077 EUR = 0.0115 CAD
// function convert(amount: number, devise: string) {
//   // Demander à l'utilisateur de rentrer le montant
//   // Demander à l'utilisateur de choisir la devise
//   // If EUR -> renvoyer correspondance en JPY et CAD
//   // If JPY -> renvoyer correspodnance en EUR et CAD
//   // If CAD -> renvoyer correspondance en JPY et EUR
// }
// convert(12, 'JPY');
// eslint-disable-next-line @typescript-eslint/no-var-requires
const readline = require('readline').createInterface({
    input: process.stdin,
    output: process.stdout,
});
readline.question('Who are you?', (name) => {
    console.log(`Hey there ${name}!`);
    readline.close();
});
