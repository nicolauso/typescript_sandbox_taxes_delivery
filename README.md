# PRESENTATION DU PROJET
*typescript_sandbox_taxes_delivery* est un exercice d'entraînement pour apprendre à utiliser TypeScript.

Il s'agit de créer plusieurs fonctions qui permettent de définir les taxes à payer pour l'envoi de colis depuis la France vers le Canada ou le Japon.
- Les taxes dépendent du poids, de la destination, de la hauteur et de la valeur du colis.
- La conversion des devises se fait toujours depuis l'euro et dépend du pays de destination.

Les données d'entrées proviennent du terminal, plus précisément des inputs complétées par les users. Des questions apparaissent dans le terminal, auxquelles les users peuvent répondre : poids du colis, valeur du colis, pays de destination...

# INSTALLATION 
- Cloner le repository "typescript_sandbox_taxes_delivery"
- Installer les dépendances  : epuis le repository, entrer la ligne de commande ```npm ci``` 


# LANCEMENT
- Dans le terminal, lancer la ligne de commande suivante :  ``` npx ts-node ./src/index.ts ```

# UTILISATION
- À chaque question, compléter l'input avec une réponse au format attendu. Valider avec le bouton Entrée.
- Possibilité de quitter l'interface en pressant la lettre "q". 
- À la fin du "questionnaire", un message affiche le montant total des taxes pour le colis à livrer. 