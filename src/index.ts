const readline = require('readline');

//Fonction qui convertit une somme d'une devise vers 2 autres (JPY, CAD, EUR)
// Inputs : entrées par l'user : amount, devise du montant (JPY, CAD, EUR)
function convert(amount: number, devise: string) {
  if (devise == 'EUR') {
    const euroToJPY = amount * 130;
    const euroToCAN = amount * 1.5;
    console.log(`${amount} ${devise} = ${euroToJPY} JPY`);
    console.log(`${amount} ${devise} = ${euroToCAN} CAN`);
  } else if (devise == 'JPY') {
    const JPYToEuro = amount * 0.0077;
    const JPYToCAD = amount * 0.0115;
    console.log(`${amount} ${devise} = ${JPYToEuro} Euro`);
    console.log(`${amount} ${devise} = ${JPYToCAD} CAN`);
  } else if (devise == 'CAD') {
    const CANToJPY = amount * 87;
    const CANToEuro = amount * 0.67;
    console.log(`${amount} ${devise} = ${CANToJPY} JPY`);
    console.log(`${amount} ${devise} = ${CANToEuro} Euro`);
  }
}

// Fonction qui ajoute des taxes d'envoi d'un colis en fonction de son poids, de sa hauteur et de sa destination
// Inputs : entrées par l'user : weight, height, destination
// Output : taxe totale
function addTaxes(weight: number, height: number, destination: string) {
  let taxe = 0;
  let devise = '';
  switch (destination) {
    case 'France':
      if (weight < 1) {
        taxe = taxe += 10;
      } else if (weight == 1 && weight <= 3) {
        taxe = taxe += 20;
      } else if (weight > 3) {
        taxe = taxe += 30;
      }
      if (height > 150) {
        taxe = taxe += 5;
      }
      devise = 'Euro';
      break;
    case 'Canada':
      if (weight < 1) {
        taxe = taxe += 15;
      } else if (weight == 1 && weight <= 3) {
        taxe = taxe += 30;
      } else if (weight > 3) {
        taxe = taxe += 45;
      }
      if (height > 150) {
        taxe = taxe += 7.5;
      }
      devise = 'CAD';
      break;
    case 'Japon':
      if (weight < 1) {
        taxe = taxe += 1000;
      } else if (weight == 1 && weight <= 3) {
        taxe = taxe += 2000;
      } else if (weight > 3) {
        taxe = taxe += 3000;
      }
      if (height > 150) {
        taxe = taxe += 500;
      }
      devise = 'JPY';
      break;
    default:
      console.log(
        'Sorry, the country you enter is not supported. You have to choose between France, Japon and Canada.',
      );
  }
  const totalTaxe = { taxe: taxe, devise: devise };
  return totalTaxe;
}

// Fonction qui définit le montant des taxes de douane suivant le pays de destiantion et la valeur du colis.
// Inputs : entrées par l'user : destination, value)
// Ouput : taxes de douane totale
function addDuties(destination: string, value: number) {
  let taxe = 0;
  switch (destination) {
    case 'France':
      taxe = 0;
      break;
    case 'Canada':
      if (value > 20) {
        taxe = value += 0.15 * value;
      }
      break;
    case 'Japon':
      if (value > 5000) {
        taxe = value += 0.1 * value;
      }
  }
  const addDutieTaxe = taxe;
  return addDutieTaxe;
}

// Création d'une interface d'interaction utilisateur/trice avec la méthode readline et process.stin/process.stdout
// "Ouvrent" des inputs dans le terminal pour que l'user entre des valeurs
const rl = readline.createInterface({
  input: process.stdin,
  output: process.stdout,
});

// Fonction qui crée la userinterface dans le terminal : affichage des questions
// Possibilité de quitter l'interface en pressant "q"
function ask(question: string, callback: any) {
  rl.question(question, (answer: any) => {
    if (answer.toLowerCase() === 'q') {
      process.exit(1);
    }
    callback(answer);
  });
}

// Fonction qui renvoie la conversion d'un montant d'une devise vers 2 autres devises (CAD, JPY, Euro)
// Entrées : inputs user dans le terminal => amount, devise
function convertMoney() {
  ask('What is the amount you want to convert: ', (amount: number) => {
    ask('What is your devise (JPY, EUR, CAD): ', (devise: string) => {
      convert(amount, devise);
      rl.close();
    });
  });
}

// Fonction renvoie les taxes totales liées à l'expédition d'un colis.
// Inputs: insérées par le user dans le terminal à l'affichage des question = la weight, la height, la value du package.
// Cette fonction fait appel à deux autres fonctions : addTaxes et addDuties pour définir les taxes liées aux dimensions
// et celles liées aux frais de douane.
function defineTaxes() {
  ask('What is the weight of your package: ', (weight: number) => {
    ask('What is the height of your package:', (height: number) => {
      ask(
        'What is the destination of your package: ',
        (destination: string) => {
          ask('What is the value of your package: ', (value: number) => {
            const totalTaxe = addTaxes(weight, height, destination);
            let totalDuties = addDuties(destination, value);
            const taxe = (totalDuties += totalTaxe.taxe);
            convert(taxe, totalTaxe.devise);
            console.log(
              `The taxe you are going to pay are : ${taxe} ${totalTaxe.devise}`,
            );
            rl.close();
          });
        },
      );
    });
  });
}

// L'appel de la fonction defineTaxes permet de renseigner le prix que l'on va payer pour l'envoi d'un colis.
defineTaxes();
// convertMoney();
